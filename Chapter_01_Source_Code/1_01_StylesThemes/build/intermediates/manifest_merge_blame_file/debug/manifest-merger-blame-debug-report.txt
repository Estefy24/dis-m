1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.androidrecipes.appstyles"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="7"
8-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml
9        android:targetSdkVersion="23" />
9-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml
10
11    <application
11-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:6:5-27:19
12        android:debuggable="true"
13        android:icon="@drawable/ic_launcher"
13-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:8:9-45
14        android:label="@string/app_name" >
14-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:7:9-41
15        <activity
15-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:9:9-17:20
16            android:name="com.androidrecipes.appstyles.ThemedActivity"
16-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:10:13-43
17            android:label="@string/label_theme"
17-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:12:13-48
18            android:theme="@style/AppTheme" >
18-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:11:13-44
19            <intent-filter>
19-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:13:13-16:29
20                <action android:name="android.intent.action.MAIN" />
20-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:14:17-68
20-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:14:25-66
21
22                <category android:name="android.intent.category.LAUNCHER" />
22-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:15:17-76
22-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:15:27-74
23            </intent-filter>
24        </activity>
25        <activity
25-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:18:9-26:20
26            android:name="com.androidrecipes.appstyles.StylesActivity"
26-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:19:13-43
27            android:label="@string/label_style"
27-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:21:13-48
28            android:theme="@style/Theme.AppCompat.Light.DarkActionBar" >
28-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:20:13-71
29            <intent-filter>
29-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:13:13-16:29
30                <action android:name="android.intent.action.MAIN" />
30-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:14:17-68
30-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:14:25-66
31
32                <category android:name="android.intent.category.LAUNCHER" />
32-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:15:17-76
32-->D:\Semestre_2020-2021\DISPOSITIVOS_MOVILES\LIBRO\android-recipes-5ed\android-recipes-5ed-master\Chapter_01_Source_Code\1_01_StylesThemes\src\main\AndroidManifest.xml:15:27-74
33            </intent-filter>
34        </activity>
35    </application>
36
37</manifest>
